package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

var firstConfig = Config{
	DefInt:     11,
	DefStr:     "11",
	DefFl:      11.1,
}

var secondConfig = Config{
	DefInt:     22,
	DefStr:     "22",
	DefFl:      22.2,
}

var configMap = map[string]Config{"1": firstConfig, "2": secondConfig}

type Config struct {
	ConfigType string
	DefInt int
	DefStr string
	DefFl float32
}

func (c *Config) UnmarshalJSON(b []byte) error {

	type configAlias Config // a new type has no methods defined so it avoids the recursive call...?

	var newConfig configAlias
	err := json.Unmarshal(b, &newConfig)
	if err != nil {
		return err
	}

	dc, ok := configMap[newConfig.ConfigType]
	if !ok {
		return fmt.Errorf("could not find %v in map", newConfig.ConfigType)
	}
	config := configAlias(dc)

	// Load original with defaults set
	err = json.Unmarshal(b, &config)
	if err != nil {
		return err
	}

	*c = Config(config)
	return nil
}


func main() {

	b, err := ioutil.ReadFile("cmd/config.json")
	if err != nil {
		fmt.Printf("%v \n", err)
		return
	}

	var conf Config
	if err := json.Unmarshal(b, &conf); err != nil {
		fmt.Printf("%v \n", err)
		return
	}

	fmt.Printf("config struct: %+v \n", conf)
	return

}
